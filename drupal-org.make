api = 2
core = 7.x

projects[jquery_update][type] = module
projects[jquery_update][version] = 2.3
projects[jquery_update][subdir] = contrib

projects[walkhub][type] = module
projects[walkhub][download][type] = git
projects[walkhub][download][branch] = 7.x-1.x
projects[walkhub][subdir] = contrib
